%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% DOCUMENTATION EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMAND:      \name
% BEHAVIOR:
    %   Here.
% ARGUMENTS:
    % 1 what. [More info.]
% NOTES:
    %   Here.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ENVIRONMENT:  name
% BEHAVIOR:
    %   Here.
% ARGUMENTS:
    % 1 what. [More info.]
% NOTES:
    %   Here.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{problemset}[2020-03-02 Problem set class]
\NeedsTeXFormat{LaTeX2e}

\LoadClass{article}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}

\RequirePackage{ifdraft} % for checking whether the article class was invoked in draft mode

\RequirePackage[letterpaper,margin=1in]{geometry}
\RequirePackage{silence} % for suppressing warnings
\WarningFilter{pageslts}{Package hyperref NOT detected.}
\RequirePackage{fancyhdr, authoraftertitle} % for headers and footers
\ifdraft{}{\RequirePackage{pageslts}} % for headers and footers
\RequirePackage[compact]{titlesec} % for compact section titles

\RequirePackage{xparse, etoolbox} % for better control of custom commands and environments
\RequirePackage[most]{tcolorbox} % for boxes that can expand to fill the remaining height of the current page
\RequirePackage{enumitem} % for better control of enumerate, itemize, and description
\RequirePackage{xcolor, framed} % for making shaded boxes
\RequirePackage{needspace} % for reserving space at the bottom of pages

\RequirePackage{amsmath, amssymb, mathtools} % for many mathematical environments & tools
\RequirePackage[arrowdel]{physics} % for more mathematical tools
\RequirePackage{esint} % for more mathematical tools
\RequirePackage{siunitx} % for SI units
\sisetup{per-mode=symbol} % use slash for \per instead of negative powers

\RequirePackage{tikz} % for figures
\usetikzlibrary{decorations.markings}
\RequirePackage{pgfplots} % for plotting
\pgfplotsset{compat=1.16,samples=100}

\RequirePackage{csquotes} % for replacement of "$STR" with ``$STR''
\MakeOuterQuote{"}

% Read all the options and pass them to article
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
% Class structure: execution of options part
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMAND:      \course
% BEHAVIOR:
    %   Accepts a course name and stores it as \@course.
% ARGUMENTS:
    % 1 course name
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\@course{}
\newcommand\course[1]{
    \renewcommand\@course{#1}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMAND:      \headertitle
% BEHAVIOR:
    %   Accepts a header title and stores it as \@headertitle
% ARGUMENTS:
    % 1 header title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\@headertitle{}
\newcommand\headertitle[1]{
    \renewcommand\@headertitle{#1}
}

% Set the header and footer style.
\AtBeginDocument{\pagenumbering{arabic}}
% For the first page:
\fancypagestyle{plain}{
    \renewcommand{\headrulewidth}{0pt}
    \fancyhf{}
    \fancyhead[R]{\@course}
    \fancyfoot[C]{\thepage{} of \ifdraft{??}{\pageref{pagesLTS.arabic}}}
}
% For general pages:
\pagestyle{fancy}
\fancyhead[L]{\@course}
\fancyhead[C]{\@headertitle}
\fancyhead[R]{\MyDate}
\fancyfoot[C]{\thepage{} of \ifdraft{??}{\pageref{pagesLTS.arabic}}}

\setlength{\parindent}{0em}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ENVIRONMENT:  keeptogether
% BEHAVIOR:
    %   This environment will prevent an awkward page break in the first half of its body.
    %   This may be used in conjunction with the user-accessible (read-only, please) etoolbox toggle 'keeptogetherisprintingversion' in order to specify different body behavior based on whether the body is currently being expanded for height measurement or printing. That is, the user may include in the body \iftoggle{keeptogetherisprintingversion}{output to be printed}{output to be measured}. Other etoolbox boolean logic may also be used with this toggle.
% ARGUMENTS:
    % 1 environment body.
% NOTES:
    %   Pushes the start of the body to the next page if there is not space for at least half of the body on the current page. This pushing will not occur if more than 40% of the current page is still free.
    %   The nonprinting version of the body is the version that is expanded but not displayed (it is used for measurement of body height). Because expanding the same block repeatedly can result in different output (if counters change, for example), we allow for users to provide a nonprinting body which may be separate from but parallel to the printing body.
    %   This environment may produce overfull/underfull hbox warnings. These warning may be safely ignored.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtoggle{keeptogetherisprintingversion}
\newsavebox{\keeptogethermeasurementbox}
\newsavebox{\remainingpageheightmeasurementbox}
\NewDocumentEnvironment{keeptogether}{ +b }{
    % Measure the height of the body and make accessible by \ht\keeptogethermeasurementbox.
    \togglefalse{keeptogetherisprintingversion}
    \savebox{\keeptogethermeasurementbox}{\vbox{#1}}
    % Measure the height remaining on the current page and make accessible by \ht\remainingpageheightmeasurementbox.
    \savebox{\remainingpageheightmeasurementbox}{\vbox{\begin{tcolorbox}[height fill]\end{tcolorbox}}}
    % DEBUG
    % \marginpar{\the\ht\keeptogethermeasurementbox, \the\ht\remainingpageheightmeasurementbox, \the\textheight}
    \ifdim \ht\remainingpageheightmeasurementbox < 0.4\textheight{ % if less than 40% of the current page is still free
        % Push the start of the environment to a new page if less than half of it will fit on the current page.
        % (Require a height equal to the push length to be available on the current page. Otherwise, push to a new page.)
        \needspace{0.5\ht\keeptogethermeasurementbox}
    }\else{ % if more than 40% of the current page is still free
        % Don't push.
    }\fi
    % Print the body.
    \toggletrue{keeptogetherisprintingversion}
    #1
}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMAND:      \prmsolhelper
% BEHAVIOR:
    %   Places a problem statement and solution together in an enumerate. The problem statement is numbered with either a specified string or an incrementing value. The problem statement is placed in a shaded box.
    %   This command is not to be called directly. Use the prmsol environment instead.
% ARGUMENTS:
    % 1 star (optional). Its presence indicates that the output of this command will not be printed.
    % 2 problem number/label (optional).
    % 3 first part of environment body. Prefix if overloaded, problem statement if not.
    % 4 second part of environment body. Problem statement if overloaded, solution if not.
    % 5 third part of environment body. Solution if overloaded, -NoValue- if not.
% NOTES:
    %   "Overloading" this command with a fifth argument that is not -NoValue- will allow for placement of a prefix before the problem statement.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newlist{prmsolnonprintingenumerate}{enumerate}{1}
\newlist{prmsolprintingenumerate}{enumerate}{1}
\setlist[prmsolnonprintingenumerate]{label=\arabic*.,resume=prmsolnonprintingenumerate,leftmargin=0pt}
\setlist[prmsolprintingenumerate]{label=\arabic*.,resume=prmsolprintingenumerate,leftmargin=0pt}
\NewDocumentCommand{\prmsolhelper}{ s o +m +m +m }{
    \IfValueTF{#5}{ % if overloaded
        \newcommand\problem{#4}
        \newcommand\solution{#5}
        % Include the prefix.
        #3
    }{ % if not overloaded
        \newcommand\problem{#3}
        \newcommand\solution{#4}
    }
    % Resume numbering from the prior enumerate. Keep two separate series of connected enumerates; one for non-printing calls of \prmsolhelper and one for printing calls of \prmsolhelper.
    \IfBooleanTF{#1}{\begin{prmsolnonprintingenumerate}}{\begin{prmsolprintingenumerate}}
        % Place the problem statement in a shaded box.
        \colorlet{shadecolor}{\ifdraft{red!25}{gray!25}}
        \begin{snugshade}
            % Conditionally number the item based on the optional presence of the problem number.
            \IfValueTF{#2}{\item[#2]}{\item} \problem\phantom{}
        \end{snugshade}
        \solution\phantom{}
    \IfBooleanTF{#1}{\end{prmsolnonprintingenumerate}}{\end{prmsolprintingenumerate}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMAND:      \restartprmsolcounter
% BEHAVIOR:
    %   Resets the counter used to number prmsol problems to 1.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\makeatletter
\newcommand{\restartprmsolcounter}{
    % Ideally, enumitem's \restartlist{prmsolnonprintingenumerate} (and its pair command) would accomplish this task.) However, it only sets \enit@resume@prmsolnonprintingenumerate to empty. Since we are using series to be able to resume two separate counters, we need to further set \enit@resume@series@prmsolnonprintingenumerate to empty.
    \let\enit@resume@series@prmsolnonprintingenumerate\@empty
    \let\enit@resume@series@prmsolprintingenumerate\@empty
}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ENVIRONMENT:  prmsol
% BEHAVIOR:
    %   Combines a problem and solution in one environment, attempting to prevent an awkward page break in the first half of the body.
    %   Also accepts a prefix to be attached before the problem statement.
% ARGUMENTS:
    % 1 star (optional). Its presence indicates that this environment should not manipulate page break location (to be used manually in edge cases).
    % 2 problem number/label (optional).
    % 3 environment body, split on ';;' into (up to) three separate parts. If the environment body has two parts separated by ';;', the parts will be treated as problem statement and solution. If the environment body has three parts separated by ';;', the parts will be treated as a prefix (kept on the page with the other material), problem statement, and solution.
% NOTES:
    %   Pushes the start of the body to the next page if there is not space for at least half of the body on the current page. This pushing will not occur if more than 40% of the current page is still free.
    %   This environment may produce overfull/underfull hbox warnings. These warning may be safely ignored.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newsavebox{\prmsolbox}
\NewDocumentEnvironment{prmsol}{ s o >{\SplitArgument{2}{;;}}+b }{
    \ifdraft{ % if in draft mode, do not use keeptogether
        \prmsolhelper[#2] #3
    }{
    \IfBooleanTF{#1}{ % if star present, do not use keeptogether
        \prmsolhelper[#2] #3
    }{ % if star not present, use keeptogether
        \begin{keeptogether}
            \iftoggle{keeptogetherisprintingversion}{
                \prmsolhelper[#2] #3
            }{
                \prmsolhelper*[#2] #3
            }
        \end{keeptogether}
    }
    }
}{}
