# $`\text{\LaTeX}`$
A collection of my $`\text{\LaTeX}`$ classes and packages. These are aimed primarily for personal use, but they are free to use (gratis & libre) in accordance with the provided license.

## Installation
Either clone this repository into `~/texmf/tex/latex` or clone it to `/some/path` and create a symlink `mkdir -p ~/texmf/tex/latex && cd ~/texmf/tex/latex && ln -s /some/path ganden`.

## License
This project is licensed under the GNU AGPLv3 license. See [LICENSE](LICENSE).
